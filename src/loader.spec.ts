import { HtmlValidateVueWebpackLoader } from "./loader";
import { prettify } from "./loader-utils";

interface TestCase {
	description: string;
	source: string;
	result: string;
	fileName: string;
}

const testCases: TestCase[] = [
	{
		description: "should handle an empty source",
		source: "",
		result: prettify({ test: {} }),
		fileName: "test.json",
	},

	{
		description: "should handle empty html validate rules",
		source: "<htmlvalidate>{}</htmlvalidate>",
		result: prettify({ test: {} }),
		fileName: "test.json",
	},

	{
		description: "should handle basic html validate rules",
		source: '<htmlvalidate>{ "inherit": "button" }</htmlvalidate>',
		result: prettify({ test: { inherit: "button" } }),
		fileName: "test.json",
	},

	{
		description: "should handle basic html validate rules with name property",
		source: `
    <script>
    export default { name: "TestComponent" };
    </script>

    <htmlvalidate>
    {
      "inherit": "button"
    }
    </htmlvalidate>
    `,
		result: prettify({ "test-component": { inherit: "button" } }),
		fileName: "test-component.json",
	},

	{
		description:
			"should handle html validate rules with slot rules that used `:` as slot indicator",
		source: `
    <script>
    export default { name: "TestComponent" };
    </script>

    <htmlvalidate>
    {
      "inherit": "button",
      "slots": ["before-default", "default"],
      ":before-default": { "permittedContent": ["@phrasing"] },
      ":default": { "permittedContent": ["@phrasing"] }
    }
    </htmlvalidate>
    `,
		result: prettify({
			"test-component": {
				inherit: "button",
				slots: ["before-default", "default"],
			},
			"test-component:before-default": { permittedContent: ["@phrasing"] },
			"test-component:default": { permittedContent: ["@phrasing"] },
		}),
		fileName: "test-component.json",
	},

	{
		description:
			"should handle html validate rules with slot rules that used `#` as slot indicator",
		source: `
    <script>
    export default { name: "TestComponent" };
    </script>

    <htmlvalidate>
    {
      "inherit": "button",
      "slots": ["before-default", "default"],
      "#before-default": { "permittedContent": ["@phrasing"] },
      "#default": { "permittedContent": ["@phrasing"] }
    }
    </htmlvalidate>
    `,
		result: prettify({
			"test-component": {
				inherit: "button",
				slots: ["before-default", "default"],
			},
			"test-component#before-default": { permittedContent: ["@phrasing"] },
			"test-component#default": { permittedContent: ["@phrasing"] },
		}),
		fileName: "test-component.json",
	},
];

testCases.forEach(({ source, result, fileName, description }) => {
	it(`${description}`, () => {
		expect.assertions(2);
		const testThis = {
			emitFile: jest.fn(),
			resource: "components/Test.vue",
		};
		const loader = HtmlValidateVueWebpackLoader.bind(testThis);

		expect(loader(source)).toBe(`module.exports = ${result};`);
		expect(testThis.emitFile).toHaveBeenCalledWith(fileName, result, undefined);
	});
});
