import {
	getComponentName,
	getFileName,
	getHtmlValidateContent as getHtmlValidateBlockContent,
	toKebabCase,
	prettify,
	getHtmlValidateSlotRules,
	getHtmlValidateRootRules,
} from "./loader-utils";

/* hack until bundled ts declarations includes LoaderContext */
/* https://github.com/webpack/webpack/issues/11630 */
interface LoaderContext {
	resource: string;
	emitFile(name: string, content: Buffer | string, sourceMap: undefined): void;
}

export function HtmlValidateVueWebpackLoader(this: LoaderContext, source: string): string {
	try {
		const nameFromComponent = getComponentName(source);
		const nameFromResource = getFileName(this.resource);
		const componentName = toKebabCase(nameFromComponent || nameFromResource);

		const blockContent = getHtmlValidateBlockContent(source);
		// ? Parse and stringify htmlvalidate content so we know it is a valid JSON.
		JSON.parse(blockContent);

		const rootRules = getHtmlValidateRootRules(blockContent, componentName);
		const slotRules = getHtmlValidateSlotRules(blockContent, componentName);

		const rules = prettify({
			...rootRules,
			...slotRules,
		});

		this.emitFile(`${componentName}.json`, rules, undefined);

		return `module.exports = ${rules};`;
	} catch (error) {
		/* eslint-disable-next-line no-console -- should handle error better */
		console.error("HtmlValidateVueWebpackLoader received the following error:", error);
		return "module.exports = {};";
	}
}

export default HtmlValidateVueWebpackLoader;
