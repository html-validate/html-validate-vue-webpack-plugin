/**
 * Wrapper for `JSON.stringify(value, null, 2);`.
 * @param value - the value to prettify
 */
export function prettify(value: unknown): string {
	return JSON.stringify(value, null, 2);
}

/**
 * Takes a value and converts it to kebab-case format.
 * @param value - the value to convert
 */
export function toKebabCase(value: string): string {
	return value
		.replace(/([A-Z])([A-Z])/g, "$1-$2")
		.replace(/([a-z])([A-Z])/g, "$1-$2")
		.replace(/[\s_]+/g, "-")
		.toLowerCase();
}

/**
 * Takes a file path and returns the file name from the end of the file path.
 * @param filePath - the file path, i.e. example/file.vue or file.vue
 */
export function getFileName(filePath: string): string {
	const lastIndexOfSlash = filePath.lastIndexOf("/");
	const lastIndexOfDot = filePath.lastIndexOf(".");
	/* eslint-disable-next-line sonarjs/argument-type -- false positive */
	return filePath.slice(lastIndexOfSlash + 1, lastIndexOfDot === -1 ? undefined : lastIndexOfDot);
}

/**
 * Takes a source and returns the value of the name property.
 * If no name property is found it will return an empty string.
 * @param source - the content with a Vue SFC, i.e.
 * ```vue
 * <script>
 *   export default { name: 'Example' };
 * </script>
 * ```
 */
export function getComponentName(source: string): string {
	const regexp = new RegExp("name:\\s*[\"']?(\\w+)[\"']?");
	const result = regexp.exec(source) ?? [];
	const [, componentName = ""] = result;
	return componentName;
}

/**
 * Takes a source and returns the content of the `<htmlvalidate>{}</htmlvalidate>` block.
 * If no content is found it will return an empty string object `"{}"`.
 * @param source - the content with a htmlvalidate block, i.e.
 * ```vue
 * <htmlvalidate>
 *   {}
 * </htmlvalidate>
 * ```
 */
export function getHtmlValidateContent(source: string): string {
	const regexp = new RegExp("<htmlvalidate>((?:[\\n\\t\\r]|.)+)</htmlvalidate>");
	const result = regexp.exec(source) ?? [];

	const [, block = "{}"] = result;
	return block;
}

/**
 * Takes html validate content and returns all slot names.
 * Slot names are prefixed with either `#` or `:`.
 * If no slot names are found it will return an empty array.
 * @param htmlValidateContent - @see getHtmlValidateContent
 */
export function getHtmlValidateSlotNames(htmlValidateContent: string): string[] {
	/* eslint-disable-next-line sonarjs/single-character-alternation -- technical debt */
	const regexp = new RegExp("(#|:)([\\w-]+)", "g");
	return htmlValidateContent.match(regexp) ?? [];
}

/**
 * Takes html validate content and returns all the rules that should be in the root, i.e. no slot rules.
 * @param htmlValidateContent - @see getHtmlValidateContent
 * @param componentName - the name of the component
 */
export function getHtmlValidateRootRules(
	htmlValidateContent: string,
	componentName: string,
): Record<string, unknown> {
	const htmlValidateContentParsed = JSON.parse(htmlValidateContent) as Record<string, unknown>;
	const slotNames = getHtmlValidateSlotNames(htmlValidateContent);

	/* eslint-disable-next-line @typescript-eslint/no-dynamic-delete -- technical debt, should filter based on keys instead */
	slotNames.forEach((slotName) => delete htmlValidateContentParsed[slotName]);

	const rootRules: Record<string, unknown> = {};
	rootRules[componentName] = htmlValidateContentParsed;

	return rootRules;
}

/**
 * Takes html validate content and returns all the rules that are considered as slot rules.
 * @param htmlValidateContent - @see getHtmlValidateContent
 * @param componentName - the name of the component
 */
export function getHtmlValidateSlotRules(
	htmlValidateContent: string,
	componentName: string,
): Record<string, unknown> {
	const htmlValidateContentParsed = JSON.parse(htmlValidateContent) as Record<string, unknown>;
	const slotNames = getHtmlValidateSlotNames(htmlValidateContent);

	const slotRules: Record<string, unknown> = {};

	slotNames.forEach((slotName) => {
		slotRules[`${componentName}${slotName}`] = htmlValidateContentParsed[slotName] ?? {};
	});

	return slotRules;
}
