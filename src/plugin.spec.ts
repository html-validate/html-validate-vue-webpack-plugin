import { Compiler, RuleSetRule } from "webpack";
import { HtmlValidateVueWebpackPlugin } from "./plugin";
import { webpackConfig } from "./webpack-config";

it("should have a constructor", () => {
	expect.assertions(1);
	const plugin = new HtmlValidateVueWebpackPlugin();
	expect(plugin).toBeInstanceOf(HtmlValidateVueWebpackPlugin);
});

it("should have an apply function", () => {
	expect.assertions(1);
	const plugin = new HtmlValidateVueWebpackPlugin();
	expect(plugin.apply).toBeInstanceOf(Function);
});

it("should add webpack config to the compiler", () => {
	expect.assertions(1);
	const plugin = new HtmlValidateVueWebpackPlugin();
	const testCompiler = {
		options: { module: { rules: [] as RuleSetRule[] } },
	} as Compiler;
	plugin.apply(testCompiler);

	expect(testCompiler.options).toMatchObject(webpackConfig);
});
