import * as path from "path";
import { type Configuration } from "webpack";

export const webpackConfig: Configuration = {
	module: {
		rules: [
			{
				test: /\.vue$/,
				use: path.resolve(__dirname, "loader"),
			},
		],
	},
};
