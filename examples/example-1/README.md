# Example 1

Install dependencies:

```bash
npm install
```

Build html-validate rules from `.vue` file:

```bash
npm run build
```

Run html-validate:

```bash
npm run html-validate
```
