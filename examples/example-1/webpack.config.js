const path = require("path");
const glob = require("glob");
const { HtmlValidateVueWebpackPlugin } = require("html-validate-vue-webpack-plugin");

const entry = glob.sync("./**/*.vue").map((it) => path.resolve(it));

module.exports = {
	mode: "production",
	entry,
	output: {
		path: path.resolve(__dirname, "elements"),
		filename: "index.js",
	},
	plugins: [new HtmlValidateVueWebpackPlugin()],
};
