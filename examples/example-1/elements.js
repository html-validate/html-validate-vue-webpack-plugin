const path = require("path");
const glob = require("glob");

const elements = ["html5", ...glob.sync("./elements/*.json").map((it) => path.resolve(it))];

module.exports = { elements };
