# Examples

Examples of how html-validate-webpack-plugin can be used.

## Prerequisites

Build lib:

```bash
npm run build
```

## Example 1

An example with glob pattern to automatically use generated html-validate rules. See [README](./example-1/README.md) for details.
