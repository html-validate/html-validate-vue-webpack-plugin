# html-validate-vue-webpack-plugin changelog

## 5.0.0 (2024-12-25)

### ⚠ BREAKING CHANGES

- **deps:** require nodejs v18 or later

### Features

- **deps:** require nodejs v18 or later ([327d4f1](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/commit/327d4f1a3ee98eccc836478d9452f326b0e53410))

## [4.0.0](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/compare/v3.0.0...v4.0.0) (2023-06-04)

### ⚠ BREAKING CHANGES

- **deps:** require nodejs v16 or later

### Features

- **deps:** require nodejs v16 or later ([0333dcb](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/commit/0333dcbdb9e2be84e5d18d7bad99421a81a7504f))

## [3.0.0](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/compare/v2.0.3...v3.0.0) (2022-05-09)

### ⚠ BREAKING CHANGES

- require node 14

### Features

- require node 14 ([467e887](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/commit/467e887ee1f83855da0c6cc3a91e69c338a623e6))

### [2.0.3](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/compare/v2.0.2...v2.0.3) (2022-01-05)

### Dependency upgrades

- **deps:** update dependency schema-utils to v4 ([3b4f158](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/commit/3b4f158747ec6c6731fb63fd8884d4fd0678c69f))

### [2.0.2](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/compare/v2.0.1...v2.0.2) (2021-11-14)

### Dependency upgrades

- **deps:** update dependency json-schema to ^0.4.0 ([1ef62e5](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/commit/1ef62e542a6a3ca88d9bcccf0acf4fba869e6cd0))

### [2.0.1](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/compare/v2.0.0...v2.0.1) (2021-07-04)

### Reverts

- Revert "chore(renovate): set updateInternalDeps" ([a946185](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/commit/a946185bfceb4d8d5111539165650058c68da6c4))

## [2.0.0](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/compare/v1.0.3...v2.0.0) (2021-06-20)

### ⚠ BREAKING CHANGES

- require NodeJS 12

### Features

- require NodeJS 12 ([0c785ae](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/commit/0c785aed61510c4aafc7637371bc6ccd5aab7e4e))

### [1.0.3](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/compare/v1.0.2...v1.0.3) (2021-02-21)

### Dependency upgrades

- **deps:** update dependency webpack to v5 ([529d3e1](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/commit/529d3e13a1adb5af81cb09d69243266037736d56))

### [1.0.2](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/compare/v1.0.1...v1.0.2) (2021-01-10)

### Dependency upgrades

- **deps:** update dependency json-schema to ^0.3.0 ([0b085f1](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/commit/0b085f1783472ca7ed591e43969b592a100b479e))

### [1.0.1](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/compare/v1.0.0...v1.0.1) (2020-11-08)

### Dependency upgrades

- **deps:** migrate to schema-utils v3 ([a67c8fc](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/commit/a67c8fc80a850245241cc68b395ac7d71caebc8f))
- **deps:** update dependency schema-utils to v3 ([ddf7687](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/commit/ddf76870162d3fe345a9af0a4f137221c73d0959))

# [1.0.0](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/compare/v0.5.1...v1.0.0) (2020-05-28)

### Features

- force 1.0.0 release ([43d77fe](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/commit/43d77fec8b9c0605d2ece5ddef452a2e823f2904))

### BREAKING CHANGES

- Nothing, this commit only forces a major 1.0.0
  release.

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 0.5.1 (2020-04-06)

### Features

- add first working prototype ([326519b](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/commit/326519be6d5e2d40423e18cd6d56b0d4efbe9542))
- init project ([6cbb994](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/commit/6cbb99410dc05c7b1e12f86dc3eeef4c6f0f398a))
- ongoing development ([7f7ac97](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/commit/7f7ac97b0a19a6ceec00335e607338ce17b70d00))
- setup initial webpack dependencies ([3e3e06b](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/commit/3e3e06b4e1f9c24b173ac9faa6bf01ff55600ed1))
- support writing slot rules ([63db218](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/commit/63db2181f1c7fd1f50c57aeebe0f52449b8d1963))
- update package name ([34fcbe0](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/commit/34fcbe0640d7aef9ea9112d51d64246defcf4c3c))

### Bug Fixes

- add comments to gitmessage template ([d1e15af](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/commit/d1e15af0e3555a5f40b191465a0c1c6a9be510f7))

## [0.5.0](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/compare/v0.4.7...v0.5.0) (2020-04-02)

### [0.4.7](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/compare/v0.4.6...v0.4.7) (2020-04-02)

### Features

- support writing slot rules ([52747ad](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/commit/52747adc1b01e35117f3864544ba484cf19ac441))

### [0.4.6](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/compare/v0.4.5...v0.4.6) (2020-03-31)

### [0.4.5](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/compare/v0.4.4...v0.4.5) (2020-03-29)

### [0.4.4](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/compare/v0.4.3...v0.4.4) (2020-03-29)

### [0.4.3](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/compare/v0.4.2...v0.4.3) (2020-03-02)

### Features

- add first working prototype ([3691ebc](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/commit/3691ebca8d363ab91b061d803130df676d92cbcd))

### [0.4.2](https://gitlab.com/html-validate/html-validate-vue-webpack-plugin/compare/v0.4.1...v0.4.2) (2020-03-01)

### Features

- ongoing development f524f32

### 0.4.1 (2020-03-01)

### Features

- init project f4c0c8f
- setup initial webpack dependencies d9656aa
- update package name 366b573

### Bug Fixes

- add comments to gitmessage template 7954983
