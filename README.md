# html-validate-vue-webpack-plugin

> extract html-validate rules from Vue single file components

## Usage

### Installation

Install the package and its peer dependencies:

```bash
npm install --save-dev html-validate-vue-webpack-plugin webpack
```

### Configuration

Import the plugin and add it to your Webpack plugins:

```js
const { HtmlValidateVueWebpackPlugin } = require("html-validate-vue-webpack-plugin");

module.exports = {
  plugins: [new HtmlValidateVueWebpackPlugin()],
};
```

For more advanced configuration check out the [examples](./examples/README.md).

## How it works

The package consist of three pieces, a webpack config, a webpack loader and a webpack plugin.

The webpack config consist of rules of how to handle `.vue` files.

The webpack loader handles the processing `.vue` files. For each file it will look for a `<htmlvalidate>{}</htmlvalidate>` block and assumes the JSON inside the tags is the html-validate rule for that component.
The webpack loader will automatically look for the name for the html-validate rule. First it looks at the `name` property in the Vue file and will fallback to the filename. The name of the rule will be converted to `kebab-case`.

The webpack plugin combines the webpack config and webpack loader and applies the rules and processing to the your webpack config.

## [Examples](./examples/README.md)

## LICENSE

MIT
